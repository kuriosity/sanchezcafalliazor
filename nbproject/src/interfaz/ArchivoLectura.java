/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;
import java.io.*;
import java.nio.file.Paths;
import java.util.Scanner;
/**
 *
 * @author valentin
 */
public class ArchivoLectura {
    private String linea;
    private Scanner in;
    
    public ArchivoLectura(String unNombre){
    try{
    in = new Scanner(Paths.get(unNombre));
    
    
    }
    catch(IOException e){
    System.err.println("Error ");
    System.exit(1);
    }
    
    }
    public boolean hayMasLineas(){
    boolean hay = false;
    if(in.hasNext()){
    linea = in.nextLine();
    hay = true;
    }
    return hay;
    
    }
    public String linea(){
    return linea;
    
    }
    public void cerrar(){
    in.close();
    }
}
