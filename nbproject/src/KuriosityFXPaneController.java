/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author V Sanchez Barre
 */
public class KuriosityFXPaneController extends AnchorPane {

    public KuriosityFXPaneController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("KuriosityFXPane.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void handleButton(ActionEvent event) throws IOException {
        String archivoSeleccionado;
        Parent parent1 = FXMLLoader.load(getClass().getResource("FXMLEscena2.fxml"));
        Scene scenex = new Scene(parent1);

        //get stage info
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        

        //Seleccionar archivo preguntas
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(null);

        if (selectedFile != null) {
            archivoSeleccionado = selectedFile.getPath();
            window.setScene(scenex);
            window.show();
        } else {
            System.out.println("File not valid,choose gift file");
        }

    }
    
    @FXML private javafx.scene.control.Button closeButton;
//Evento cerrar ventana
    @FXML
    public void closeButtonAction(ActionEvent event) throws IOException {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    @FXML
    private void initialize() {
    }
}
