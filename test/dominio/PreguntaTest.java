/*
 * Valentin Sanchez Barre 203458 Valentin Azor 220971 Luca Cafalli 192119
 */
package dominio;

import java.util.ArrayList;
import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;

public class PreguntaTest {

    public PreguntaTest() {
    }

    /**
     * Test of setRespuestaUsuario method, of class Pregunta.
     */
    @Test
    public void testSetRespuestaUsuario() {
        System.out.println("setRespuestaUsuario");
        String respuestaUsuario = "";
        Pregunta instance = new Pregunta();
        instance.setRespuestaUsuario(respuestaUsuario);
        assertEquals("", instance.getRespuestaUsuario());

    }

    /**
     * Test of getRespuestaUsuario method, of class Pregunta.
     */
    @Test
    public void testGetRespuestaUsuario() {
        System.out.println("getRespuestaUsuario");
        Pregunta instance = new Pregunta();
        String expResult = "";
        String result = instance.getRespuestaUsuario();
        assertEquals(expResult, result);

    }

    /**
     * Test of isRespondida method, of class Pregunta.
     */
    @Test
    public void testIsRespondida() {
        System.out.println("isRespondida");
        Pregunta instance = new Pregunta();
        boolean expResult = false;
        boolean result = instance.isRespondida();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRespondida method, of class Pregunta.
     */
    @Test
    public void testSetRespondida() {
        System.out.println("setRespondida");
        boolean respondida = false;
        Pregunta instance = new Pregunta();
        instance.setRespondida(respondida);
        assertEquals(false, instance.isRespondida());

    }

    /**
     * Test of setColor method, of class Pregunta.
     */
    @Test
    public void testSetColor() {
        System.out.println("setColor");
        String color = "#ffff";
        Pregunta instance = new Pregunta();
        instance.setColor(color);
        assertEquals(color, instance.getColor());

    }

    /**
     * Test of getColor method, of class Pregunta.
     */
    @Test
    public void testGetColor() {
        System.out.println("getColor");
        Pregunta instance = new Pregunta();
        String expResult = "";
        String result = instance.getColor();
        assertEquals(expResult, result);

    }

    /**
     * Test of setOpcion1 method, of class Pregunta.
     */
    @Test
    public void testSetOpcion1() {

        System.out.println("setOpcion1");

        String opcion1 = "";

        Pregunta instance = new Pregunta();

        instance.setOpcion1(opcion1);

        assertEquals("", instance.getOpcion1());

    }

    /**
     * Test of setOpcion2 method, of class Pregunta.
     */
    @Test
    public void testSetOpcion2() {

        System.out.println("setOpcion2");

        String opcion2 = "";

        Pregunta instance = new Pregunta();

        instance.setOpcion2(opcion2);

        assertEquals("", instance.getOpcion2());

    }

    /**
     * Test of setOpcion3 method, of class Pregunta.
     */
    @Test
    public void testSetOpcion3() {

        System.out.println("setOpcion3");

        String opcion3 = "";

        Pregunta instance = new Pregunta();

        instance.setOpcion3(opcion3);

        assertEquals("", instance.getOpcion3());

    }

    /**
     * Test of setOpcion4 method, of class Pregunta.
     */
    @Test
    public void testSetOpcion4() {
        System.out.println("setOpcion4");
        String opcion4 = "";
        Pregunta instance = new Pregunta();
        instance.setOpcion4(opcion4);
        assertEquals("", instance.getOpcion4());

    }

    /**
     * Test of setIn method, of class Pregunta.
     */
    @Test
    public void testSetIn() {
        System.out.println("setIn");
        Scanner in = null;
        Pregunta instance = new Pregunta();
        instance.setIn(in);
        assertEquals(in, instance.getIn());

    }

    /**
     * Test of getOpcion1 method, of class Pregunta.
     */
    @Test
    public void testGetOpcion1() {
        System.out.println("getOpcion1");
        Pregunta instance = new Pregunta();
        instance.setOpcion1("suarez");
        String expResult = "suarez";
        String result = instance.getOpcion1();
        assertEquals(expResult, result);

    }

    /**
     * Test of getOpcion2 method, of class Pregunta.
     */
    @Test
    public void testGetOpcion2() {
        System.out.println("getOpcion2");
        Pregunta instance = new Pregunta();
        String expResult = "suarez";
        instance.setOpcion2("suarez");
        String result = instance.getOpcion2();
        assertEquals(expResult, result);

    }

    /**
     * Test of getOpcion3 method, of class Pregunta.
     */
    @Test
    public void testGetOpcion3() {
        System.out.println("getOpcion3");
        Pregunta instance = new Pregunta();
        String expResult = "";
        String result = instance.getOpcion3();
        assertEquals(expResult, result);

    }

    /**
     * Test of getOpcion4 method, of class Pregunta.
     */
    @Test
    public void testGetOpcion4() {
        System.out.println("getOpcion4");
        Pregunta instance = new Pregunta();
        String expResult = "";
        String result = instance.getOpcion4();
        assertEquals(expResult, result);

    }

    /**
     * Test of isVerd method, of class Pregunta.
     */
    @Test
    public void testIsVerd() {
        System.out.println("isVerd");
        Pregunta instance = new Pregunta();
        boolean expResult = false;
        boolean result = instance.isVerd();
        assertEquals(expResult, result);

    }

    /**
     * Test of setVerd method, of class Pregunta.
     */
    @Test
    public void testSetVerd() {
        System.out.println("setVerd");
        boolean verd = false;
        Pregunta instance = new Pregunta();
        instance.setVerd(verd);
        assertEquals(false, instance.isVerd());

    }

    /**
     * Test of isMultOp method, of class Pregunta.
     */
    @Test
    public void testIsMultOp() {
        System.out.println("isMultOp");
        Pregunta instance = new Pregunta();
        boolean expResult = false;
        boolean result = instance.isMultOp();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMultOp method, of class Pregunta.
     */
    @Test
    public void testSetMultOp() {
        System.out.println("setMultOp");
        boolean multOp = false;
        Pregunta instance = new Pregunta();
        instance.setMultOp(multOp);
        assertEquals(false, instance.isMultOp());

    }

    /**
     * Test of isRespCorta method, of class Pregunta.
     */
    @Test
    public void testIsRespCorta() {
        System.out.println("isRespCorta");
        Pregunta instance = new Pregunta();
        boolean expResult = false;
        boolean result = instance.isRespCorta();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRespCorta method, of class Pregunta.
     */
    @Test
    public void testSetRespCorta() {
        System.out.println("setRespCorta");
        boolean respCorta = false;
        Pregunta instance = new Pregunta();
        instance.setRespCorta(respCorta);
        assertEquals(false, instance.isRespCorta());

    }

    /**
     * Test of getPregunta method, of class Pregunta.
     */
    @Test
    public void testGetPregunta() {
        System.out.println("getPregunta");
        Pregunta instance = new Pregunta();
        String expResult = "";
        String result = instance.getPregunta();
        assertEquals(expResult, result);

    }

    /**
     * Test of setPregunta method, of class Pregunta.
     */
    @Test
    public void testSetPregunta() {
        System.out.println("setPregunta");
        String pregunta = "";
        Pregunta instance = new Pregunta();
        instance.setPregunta(pregunta);
        assertEquals("", instance.getPregunta());

    }

    /**
     * Test of getRespuesta method, of class Pregunta.
     */
    @Test
    public void testGetRespuesta() {
        System.out.println("getRespuesta");
        Pregunta instance = new Pregunta();
        String expResult = "";
        String result = instance.getRespuesta();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRespuesta method, of class Pregunta.
     */
    @Test
    public void testSetRespuesta() {
        System.out.println("setRespuesta");
        String respuesta = "";
        Pregunta instance = new Pregunta();
        instance.setRespuesta(respuesta);
        assertEquals("", instance.getRespuesta());

    }

    /**
     * Test of getOpciones method, of class Pregunta.
     */
    @Test
    public void testGetOpciones() {
        System.out.println("getOpciones");
        Pregunta instance = new Pregunta();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getOpciones();
        assertEquals(expResult, result);

    }

    /**
     * Test of setOpciones method, of class Pregunta.
     */
    @Test
    public void testSetOpciones() {
        System.out.println("setOpciones");
        ArrayList<String> opciones = null;
        Pregunta instance = new Pregunta();
        instance.setOpciones(opciones);
        assertEquals(opciones, instance.getOpciones());

    }

    /**
     * Test of main method, of class Pregunta.
     */
    @Test
    public void testMain() throws Exception {
        System.out.println("main");
        String[] args = null;
        Pregunta.main(args);
    }

    @Test
    public void testToString() {
        System.out.println("test to string");
        Pregunta preguntaMO = new Pregunta();
        preguntaMO.setMultOp(true);
        Pregunta preguntaVOF = new Pregunta();
        preguntaVOF.setVerd(true);
        Pregunta preguntaRc = new Pregunta();
        preguntaRc.setRespCorta(true);
        Pregunta preguntaN = new Pregunta();

        assertEquals("Pregunta: " + preguntaMO.getPregunta() + " Respuesta: " + preguntaMO.getRespuesta() + " OP1 : " + preguntaMO.getOpcion1() + " OP2 :" + preguntaMO.getOpcion2() + " OP3: " + preguntaMO.getOpcion3() + " OP4 " + preguntaMO.getOpcion4() + " Resp usuario:" + preguntaMO.getRespuestaUsuario(), preguntaMO.toString());
        assertEquals("Pregunta: " + preguntaVOF.getPregunta() + " Respuesta: " + preguntaVOF.getRespuesta() + " Resp usuario:" + preguntaVOF.getRespuestaUsuario(), preguntaVOF.toString());
        assertEquals("Pregunta: " + preguntaRc.getPregunta() + " Opciones: " + preguntaRc.getOpciones() + " Resp usuario:" + preguntaRc.getRespuestaUsuario(), preguntaRc.toString());
        assertEquals(null, preguntaN.toString());
    }

}
