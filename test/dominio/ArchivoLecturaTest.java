/*
 * Valentin Sanchez Barre 203458 Valentin Azor 220971 Luca Cafalli 192119
 */
package dominio;

import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author V Sanchez Barre
 */
public class ArchivoLecturaTest {

    public ArchivoLecturaTest() {
    }

    /**
     * Test of hayMasLineas method, of class ArchivoLectura.
     */
    @Test
    public void testHayMasLineas() {
        System.out.println("hayMasLineas");
        ArchivoLectura instance = new ArchivoLectura();
        boolean expResult = false;
        boolean result = instance.hayMasLineas();
        assertEquals(expResult, result);

    }

    /**
     * Test of linea method, of class ArchivoLectura.
     */
    @Test
    public void testLinea() {
        System.out.println("linea");
        ArchivoLectura instance = new ArchivoLectura();
        String expResult = "";
        String result = instance.linea();
        assertEquals(expResult, result);

    }

    /**
     * Test of cerrar method, of class ArchivoLectura.
     */
    @Test
    public void testCerrar() {
        System.out.println("cerrar");
        ArchivoLectura instance = new ArchivoLectura("C:\\Users\\ValentinAdmin\\Documents\\NetBeansProjects\\proyectoprincipal\\test\\dominio\\PreguntaVOF.txt");
        instance.cerrar();

    }

    @Test
    public void testSetLinea() {
        System.out.println("set linea");
        String linea = "prueba";
        ArchivoLectura instance = new ArchivoLectura();
        instance.setLinea(linea);
        assertEquals(linea, instance.getLinea());

    }

    @Test
    public void testGetLinea() {
        System.out.println("get linea");
        String linea = "prueba";
        ArchivoLectura instance = new ArchivoLectura();
        instance.setLinea(linea);
        assertEquals("prueba", instance.getLinea());

    }
    
    @Test
    public void testGetIn(){
        System.out.println("get in");
        Scanner in = new Scanner(System.in);
        ArchivoLectura instance = new ArchivoLectura();
        instance.setIn(in);
        assertEquals(in,instance.getIn());
        
    }
    
    @Test
    public void testSetIn(){
        System.out.println("set in");
        Scanner in = new Scanner(System.in);
        ArchivoLectura instance = new ArchivoLectura();
        instance.setIn(in);
        assertEquals(in,instance.getIn());
    }

}
