/*
 * Valentin Sanchez Barre 203458 Valentin Azor 220971 Luca Cafalli 192119
 */
package dominio;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {

    public SistemaTest() {

    }

    @Before
    public void setUp() {
    }

    /**
     * Test of setCantidadPreguntasCorrectas method, of class Sistema.
     */
    @Test
    public void testSetCantidadPreguntasCorrectas() {
        System.out.println("setCantidadPreguntasCorrectas");
        int cantidadPreguntasCorrectas = 0;
        Sistema instance = new Sistema();
        instance.setCantidadPreguntasCorrectas(cantidadPreguntasCorrectas);
        assertEquals(0, instance.getCantidadPreguntasCorrectas());

    }

    /**
     * Test of getCantidadPreguntasCorrectas method, of class Sistema.
     */
    @Test
    public void testGetCantidadPreguntasCorrectas() {
        System.out.println("getCantidadPreguntasCorrectas");
        Sistema instance = new Sistema();
        int expResult = 0;
        int result = instance.getCantidadPreguntasCorrectas();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCantidadPreguntasRespondidas method, of class Sistema.
     */
    @Test
    public void testGetCantidadPreguntasRespondidas() {
        System.out.println("getCantidadPreguntasRespondidas");
        Sistema instance = new Sistema();
        int expResult = 0;
        int result = instance.getCantidadPreguntasRespondidas();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCantidadPreguntasRespondidas method, of class Sistema.
     */
    @Test
    public void testSetCantidadPreguntasRespondidas() {
        System.out.println("setCantidadPreguntasRespondidas");
        int cantidadPreguntasRespondidas = 0;
        Sistema instance = new Sistema();
        instance.setCantidadPreguntasRespondidas(cantidadPreguntasRespondidas);
        assertEquals(0, instance.getCantidadPreguntasRespondidas());

    }

    /**
     * Test of setListaPreguntas method, of class Sistema.
     */
    @Test
    public void testSetListaPreguntas() {
        System.out.println("setListaPreguntas");
        ArrayList<Pregunta> listaPreguntas = null;
        Sistema instance = new Sistema();
        instance.setListaPreguntas(listaPreguntas);
        assertEquals(listaPreguntas, instance.getListaPreguntas());

    }

    /**
     * Test of getListaPreguntas method, of class Sistema.
     */
    @Test
    public void testGetListaPreguntas() {
        System.out.println("getListaPreguntas");
        Sistema instance = new Sistema();
        ArrayList<Pregunta> expResult = new ArrayList();
        ArrayList<Pregunta> result = instance.getListaPreguntas();
        assertEquals(expResult, result);

    }

    /**
     * Test of verdadero method, of class Sistema.
     */
    @Test
    public void testVerdadero() {
        System.out.println("verdadero");
        Pregunta p = new Pregunta();
        String linea = "uno mas uno es dos {T}";
        Sistema instance = new Sistema();
        instance.verdadero(p, linea);
        assertTrue(p.isVerd());
    }

    /**
     * Test of falso method, of class Sistema.
     */
    @Test
    public void testFalso() {
        System.out.println("falso");
        Pregunta p = new Pregunta();
        String linea = "uno mas uno es dos {T}";
        Sistema instance = new Sistema();
        instance.falso(p, linea);
        assertTrue(p.isVerd());
    }

    /**
     * Test of multOpcion method, of class Sistema.
     */
    @Test
    public void testMultOpcion() {
        System.out.println("multOpcion");
        Pregunta p = new Pregunta();
        String linea = "Que color hay entre el naranja y el verde en el espectro? { =Amarillo ~rojo ~azul ~verde}";
        Sistema instance = new Sistema();
        instance.multOpcion(p, linea);

    }

    /**
     * Test of respCorta method, of class Sistema.
     */
    @Test
    public void testRespCorta() {
        System.out.println("respCorta");
        Pregunta p = new Pregunta();
        String linea = "el maximo goleador historico de la seleccion es... {=suarez}";
        Sistema instance = new Sistema();
        instance.respCorta(p, linea);
        assertTrue(p.isRespCorta());
    }

    /**
     * Test of crearPDF method, of class Sistema.
     */
    @Test
    public void testCrearPDF() throws Exception {
        System.out.println("crearPDF");
        Sistema s = new Sistema();
        Sistema.crearPDF(s);

    }

}
