
import dominio.Pregunta;
import java.io.File;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import dominio.Sistema;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 *
 * @author Valentin Sanchez, Valentin Azor, Luca Cafalli
 */
public class KuriosityFXPaneController extends AnchorPane {

    @FXML
    ImageView gifImage;

    @FXML
    Button btnMostrarGif;

    Sistema sistemaPrincipal;
    
    private int contador = 0;

    public KuriosityFXPaneController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("KuriosityFXPane.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    //accion del boton inicar juego, inica el sistema a partir de un archivo txt gift
    @FXML
    public void handleButton(ActionEvent event) throws IOException {

        String pathArchivoSeleccionado;

        //get stage info
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        //Seleccionar archivo preguntas
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(null);
        fc.setTitle("Seleccione un archivo Gift con las preguntas");

        if (selectedFile != null) {

            sistemaPrincipal = new Sistema();
            
            pathArchivoSeleccionado = selectedFile.getPath();
            
            sistemaPrincipal.tipoPregunta(pathArchivoSeleccionado);

            mostrarEscena(window,contador);
            
        } 
        

    }

    //muestra la escena numerada, en una ventana a eleccion
    public void mostrarEscena(Stage window, int numero) throws IOException {
        
        Pregunta unaPregunta = sistemaPrincipal.getListaPreguntas().get(numero);
        
        boolean respondida = unaPregunta.isRespondida();

        if (respondida == false) {
            
            if (unaPregunta.isMultOp()) {

                FXMLLoader loader = new FXMLLoader();

                loader.setLocation(getClass().getResource("FXMLEscena2.fxml"));

                Parent parentMultipleOpcion = loader.load();

                Scene sceneMultipleOpcion = new Scene(parentMultipleOpcion);

                FXMLEscena2Controller controladorEscena2 = loader.getController();

                controladorEscena2.initData(sistemaPrincipal, numero);

                window.setScene(sceneMultipleOpcion);
                window.show();
            }

            if (unaPregunta.isVerd()) {

                FXMLLoader loader = new FXMLLoader();

                loader.setLocation(getClass().getResource("FXMLEscena3.fxml"));

                Parent parentVerdaderoFalso = loader.load();

                Scene sceneVerdaderoFalso = new Scene(parentVerdaderoFalso);

                FXMLEscena3Controller controlador = loader.getController();

                controlador.initData(sistemaPrincipal, numero);

                window.setScene(sceneVerdaderoFalso);
                window.show();

            }
            if (unaPregunta.isRespCorta()) {

                FXMLLoader loader = new FXMLLoader();

                loader.setLocation(getClass().getResource("FXMLEscena4.fxml"));

                Parent parentRespuestaCorta;

                parentRespuestaCorta = loader.load();

                Scene sceneRespuestaCorta = new Scene(parentRespuestaCorta);

                FXMLEscena4Controller controlador = loader.getController();
                controlador.initData(sistemaPrincipal, numero);

                window.setScene(sceneRespuestaCorta);
                window.show();
            }
        } else {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getClass().getResource("FXMLPreguntaRespondida.fxml"));

            Parent parentPreguntaRespondida;

            parentPreguntaRespondida = loader.load();

            Scene scenePreguntaRespondida = new Scene(parentPreguntaRespondida);

            FXMLPreguntaRespondidaController controlador = loader.getController();

            controlador.initData(sistemaPrincipal, numero);

            window.setScene(scenePreguntaRespondida);
            window.show();
        }
    }

   
}
