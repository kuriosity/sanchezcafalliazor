
import com.jfoenix.controls.JFXButton;
import dominio.Pregunta;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Valentin SanchezBarre, Valentin Azor, Luca Cafalli
 */
public class FXMLEscena3Controller extends Ventana implements Initializable {

    private Sistema sistemaPrincipal;
    private Pregunta preguntaPrincipal;
    private String respuestaUsuario;
    private String respuestaPrincipal;
    private int numero;


    @FXML
    Label pregunta, lblTimer;
    @FXML
    Button btnVerdadero, btnFalso, btnSiguiente, btnResultados;
    @FXML
    ImageView imgGif, loserGif;
    @FXML
    JFXButton q1, q2, q3, q4, q5, q6, q7, q8, q9, q10;

    public void setPregunta(String pregunta) {
        this.pregunta.setText(pregunta);
    }
    
    
    //Inicia los datos de la ventana, tomando de sistema
    public void initData(Sistema sistema, int numeroPregunta) {

        sistemaPrincipal = sistema;
        preguntaPrincipal = sistemaPrincipal.getListaPreguntas().get(numeroPregunta);
        respuestaUsuario = preguntaPrincipal.getRespuestaUsuario();
        respuestaPrincipal = preguntaPrincipal.getRespuesta();
        numero = numeroPregunta;

        pregunta.setText(preguntaPrincipal.getPregunta());

        setearBotones();

        if (sistemaPrincipal.getCantidadPreguntasRespondidas() == sistemaPrincipal.getListaPreguntas().size() - 1) {
            btnResultados.setVisible(true);
        }

    }

    //Accion de boton de presionar una de las opciones multiples
    @FXML
    public void presionarOpcion(ActionEvent event) throws IOException {

        Button presionado = (Button) event.getSource();

        String textoBoton = presionado.getText();
        respuestaUsuario = textoBoton;

        sistemaPrincipal.setCantidadPreguntasRespondidas(sistemaPrincipal.getCantidadPreguntasRespondidas() + 1);

        preguntaPrincipal.setRespuestaUsuario(respuestaUsuario);

        preguntaPrincipal.setRespondida(true);

        if (respuestaUsuario.equals(respuestaPrincipal)) {

            btnVerdadero.setDisable(true);

            btnFalso.setDisable(true);

            imgGif.setVisible(true);

            presionado.setStyle("-fx-background-color: #FCFA74; ");

            playSound("src\\imagenes\\audioFFVII.mp3");

            pregunta.setText("Correcto!");

            pintarBoton(numero, "#26BD31");

            preguntaPrincipal.setColor("#26BD31");

            sistemaPrincipal.setCantidadPreguntasCorrectas(sistemaPrincipal.getCantidadPreguntasCorrectas() + 1);

        } else {

            btnVerdadero.setDisable(true);

            btnFalso.setDisable(true);

            playSound("src\\imagenes\\loseFF.mp3");

            pregunta.setText("Incorrecto...");

            loserGif.setVisible(true);

            presionado.setStyle("-fx-background-color: #FCFA74; ");

            pintarBoton(numero, "#EA2019");

            preguntaPrincipal.setColor("#EA2019");

        }

    }

    //pinta boton tomando su numero y un color formato #
    public void pintarBoton(int numero, String color1) {

        String color = "-fx-background-color:" + color1 + ";";

        if (numero == 0) {
            q1.setStyle(color);
        }
        if (numero == 1) {
            q2.setStyle(color);
        }
        if (numero == 2) {
            q3.setStyle(color);
        }
        if (numero == 3) {
            q4.setStyle(color);
        }
        if (numero == 4) {
            q5.setStyle(color);
        }
        if (numero == 5) {
            q6.setStyle(color);
        }
        if (numero == 6) {
            q7.setStyle(color);
        }
        if (numero == 7) {
            q8.setStyle(color);
        }
        if (numero == 8) {
            q9.setStyle(color);
        }
        if (numero == 9) {
            q10.setStyle(color);
        }

    }

    //accion de presionar los botones superiores del camino
    @FXML
    private void presionarBotonesSuperiores(ActionEvent event) throws IOException {

        JFXButton botonPresionado = (JFXButton) event.getSource();

        int numeroDeseado = Integer.parseInt(botonPresionado.getText());

        setearBotones();

        Stage stage = (Stage) q1.getScene().getWindow();

        mostrarEscena(stage, numeroDeseado, sistemaPrincipal);
    }

    //Toma informacion del sistema y setea los botones superiores, el camino de las preguntas, pintandolos
    private void setearBotones() {


        for (int i = 0; i < sistemaPrincipal.getListaPreguntas().size(); i++) {

            Pregunta unaPregunta = sistemaPrincipal.getListaPreguntas().get(i);
            pintarBoton(i, unaPregunta.getColor());

            if (Integer.parseInt(q1.getText()) == i) {
                q1.setVisible(true);
            }
            if (Integer.parseInt(q2.getText()) == i) {

                q2.setVisible(true);
            }
            if (Integer.parseInt(q3.getText()) == i) {
                q3.setVisible(true);
            }
            if (Integer.parseInt(q4.getText()) == i) {
                q4.setVisible(true);
            }
            if (Integer.parseInt(q5.getText()) == i) {
                q5.setVisible(true);
            }
            if (Integer.parseInt(q6.getText()) == i) {
                q6.setVisible(true);
            }
            if (Integer.parseInt(q7.getText()) == i) {
                q7.setVisible(true);
            }
            if (Integer.parseInt(q8.getText()) == i) {

                q8.setVisible(true);
            }
            if (Integer.parseInt(q9.getText()) == i) {

                q9.setVisible(true);
            }
            if (Integer.parseInt(q10.getText()) == i) {

                q10.setVisible(true);
            }
        }
    }

    @FXML
    public void clickResultados(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("FXMLRankingFinal.fxml"));

        Parent parentRankingFinal;

        parentRankingFinal = loader.load();

        Scene sceneRankingFinal = new Scene(parentRankingFinal);

        FXMLRankingFinalController controlador = loader.getController();

        controlador.initData(sistemaPrincipal);

        Stage window = (Stage) q1.getScene().getWindow();

        Sistema.crearPDF(sistemaPrincipal);

        window.setScene(sceneRankingFinal);
        window.show();
    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
