
import com.jfoenix.controls.JFXButton;
import dominio.Pregunta;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Valentin Sanchez, Valentin Azor, Luca Cafalli
 */
public class FXMLEscena2Controller extends Ventana implements Initializable {

    private Sistema sistemaPrincipal;
    private Pregunta preguntaPrincipal;
    private String respuestaUsuario;
    private String respuestaPrincipal;
    private int numero;

    @FXML
    private Label pregunta;
    @FXML
    private Button btnOpcion1, btnOpcion2, btnOpcion3, btnOpcion4, btnResultados;
    @FXML
    private JFXButton q1, q2, q3, q4, q5, q6, q7, q8, q9, q10;
    @FXML
    private ImageView imgGif, loserGif;

    //Inicia los datos de la ventana, tomando de sistema
    public void initData(Sistema sistema, int numeroPregunta) {

        sistemaPrincipal = sistema;
        preguntaPrincipal = sistemaPrincipal.getListaPreguntas().get(numeroPregunta);
        respuestaUsuario = preguntaPrincipal.getRespuestaUsuario();
        respuestaPrincipal = preguntaPrincipal.getRespuesta();
        numero = numeroPregunta;

        btnOpcion1.setText(preguntaPrincipal.getOpcion1());
        btnOpcion2.setText(preguntaPrincipal.getOpcion2());
        btnOpcion3.setText(preguntaPrincipal.getOpcion3());
        btnOpcion4.setText(preguntaPrincipal.getOpcion4());

        pregunta.setText(preguntaPrincipal.getPregunta());

        setearBotones();

        if (sistemaPrincipal.getCantidadPreguntasRespondidas() == sistemaPrincipal.getListaPreguntas().size() - 1) {
            btnResultados.setVisible(true);
        }
    }

    @FXML
    public void clickBotonOpcion(ActionEvent event) throws IOException {
        
        //Tomo la informacion seleccionada por el usuario
        Button botonSeleccionado = (Button) event.getSource();
        String opcionSeleccionada = (((Button) event.getSource()).getText());
        respuestaUsuario = opcionSeleccionada;

        //sumamos uno al contador de preguntas respondidas y la seteamos como respondida
        sistemaPrincipal.setCantidadPreguntasRespondidas(sistemaPrincipal.getCantidadPreguntasRespondidas() + 1);
        preguntaPrincipal.setRespondida(true);

        preguntaPrincipal.setRespuestaUsuario(respuestaUsuario);
        
        
        //Accion segun respueta correcta o incorrecta
        if (respuestaUsuario.equals(respuestaPrincipal)) {

            deshabilitarBotones();

            botonSeleccionado.setStyle("-fx-background-color: #FCFA74; ");

            pregunta.setText("Correcto!");

            imgGif.setVisible(true);

            playSound("src\\imagenes\\audioFFVII.mp3");

            pintarBoton(numero, "#26BD31");

            preguntaPrincipal.setColor("#26BD31");

            sistemaPrincipal.setCantidadPreguntasCorrectas(sistemaPrincipal.getCantidadPreguntasCorrectas() + 1);

        } else {

            deshabilitarBotones();

            botonSeleccionado.setStyle("-fx-background-color: #FCFA74; ");

            pregunta.setText("Incorrecto...");

            loserGif.setVisible(true);

            playSound("src\\imagenes\\loseFF.mp3");

            pintarBoton(numero, "#EA2019");

            preguntaPrincipal.setColor("#EA2019");

        }
    }

    //deshabilita todos los botones
    private void deshabilitarBotones() {
        btnOpcion1.setDisable(true);
        btnOpcion2.setDisable(true);
        btnOpcion3.setDisable(true);
        btnOpcion4.setDisable(true);
    }

    //setea los botones superiores, tomando informacion del sistema
    private void setearBotones() {

        for (int i = 0; i < sistemaPrincipal.getListaPreguntas().size(); i++) {

            Pregunta unaPregunta = sistemaPrincipal.getListaPreguntas().get(i);
            pintarBoton(i, unaPregunta.getColor());

            if (Integer.parseInt(q1.getText()) == i) {
                q1.setVisible(true);
            }
            if (Integer.parseInt(q2.getText()) == i) {

                q2.setVisible(true);
            }
            if (Integer.parseInt(q3.getText()) == i) {
                q3.setVisible(true);
            }
            if (Integer.parseInt(q4.getText()) == i) {
                q4.setVisible(true);
            }
            if (Integer.parseInt(q5.getText()) == i) {
                q5.setVisible(true);
            }
            if (Integer.parseInt(q6.getText()) == i) {
                q6.setVisible(true);
            }
            if (Integer.parseInt(q7.getText()) == i) {
                q7.setVisible(true);
            }
            if (Integer.parseInt(q8.getText()) == i) {

                q8.setVisible(true);
            }
            if (Integer.parseInt(q9.getText()) == i) {

                q9.setVisible(true);
            }
            if (Integer.parseInt(q10.getText()) == i) {

                q10.setVisible(true);
            }
        }
    }

    //pinta un boton a elecccion, con un color a eleccion formato #
    private void pintarBoton(int numero, String color1) {

        String color = "-fx-background-color:" + color1 + ";";

        if (numero == 0) {
            q1.setStyle(color);
        }
        if (numero == 1) {
            q2.setStyle(color);
        }
        if (numero == 2) {
            q3.setStyle(color);
        }
        if (numero == 3) {
            q4.setStyle(color);
        }
        if (numero == 4) {
            q5.setStyle(color);
        }
        if (numero == 5) {
            q6.setStyle(color);
        }
        if (numero == 6) {
            q7.setStyle(color);
        }
        if (numero == 7) {
            q8.setStyle(color);
        }
        if (numero == 8) {
            q9.setStyle(color);
        }
        if (numero == 9) {
            q10.setStyle(color);
        }

    }

    //Metodo ejecutado por botones superiores
    @FXML
    private void presionarBotonesSuperiores(ActionEvent event) throws IOException {
        JFXButton botonPresionado = (JFXButton) event.getSource();

        int numeroDeseado = Integer.parseInt(botonPresionado.getText());

        setearBotones();

        Stage stage = (Stage) q1.getScene().getWindow();

        mostrarEscena(stage, numeroDeseado, sistemaPrincipal);
    }

    //accion de clickear el boton, muestra resultados generales
    @FXML
    public void clickResultados(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("FXMLRankingFinal.fxml"));

        Parent parentRankingFinal;

        parentRankingFinal = loader.load();

        Scene sceneRankingFinal = new Scene(parentRankingFinal);

        FXMLRankingFinalController controlador = loader.getController();

        controlador.initData(sistemaPrincipal);

        Stage window = (Stage) q1.getScene().getWindow();

        Sistema.crearPDF(sistemaPrincipal);

        window.setScene(sceneRankingFinal);
        window.show();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
