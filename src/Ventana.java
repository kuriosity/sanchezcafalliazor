
import dominio.Pregunta;
import dominio.Sistema;
import java.io.File;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

/**
 *
 * Valentin SanchezBarre, Valentin Azor, Luca Cafalli
 */
public abstract class Ventana {
    
//reproduce un sonido, en un path a elegir
    public static void playSound(String musicFile) {
        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
    }

    //muestra la escena numerada, en una determinada ventana con una instancia de Sistema
    public static void mostrarEscena(Stage window, int numero, Sistema sistemaPrincipal) throws IOException {

        Pregunta unaPregunta = sistemaPrincipal.getListaPreguntas().get(numero);

        boolean respondida = unaPregunta.isRespondida();

        if (respondida == false) {

            if (unaPregunta.isMultOp()) {

                FXMLLoader loader = new FXMLLoader();

                ClassLoader classLoader = ClassLoader.getSystemClassLoader();
                loader.setLocation(classLoader.getResource("FXMLEscena2.fxml"));

                Parent parentMultipleOpcion = loader.load();

                Scene sceneMultipleOpcion = new Scene(parentMultipleOpcion);

                FXMLEscena2Controller controladorEscena2 = loader.getController();

                controladorEscena2.initData(sistemaPrincipal, numero);

                window.setScene(sceneMultipleOpcion);
                window.show();
            }

            if (unaPregunta.isVerd()) {

                FXMLLoader loader = new FXMLLoader();

                ClassLoader classLoader = ClassLoader.getSystemClassLoader();
                loader.setLocation(classLoader.getResource("FXMLEscena3.fxml"));

                Parent parentVerdaderoFalso = loader.load();

                Scene sceneVerdaderoFalso = new Scene(parentVerdaderoFalso);

                FXMLEscena3Controller controlador = loader.getController();

                controlador.initData(sistemaPrincipal, numero);

                window.setScene(sceneVerdaderoFalso);
                window.show();

            }
            if (unaPregunta.isRespCorta()) {

                FXMLLoader loader = new FXMLLoader();

                ClassLoader classLoader = ClassLoader.getSystemClassLoader();
                loader.setLocation(classLoader.getResource("FXMLEscena4.fxml"));

                Parent parentRespuestaCorta;

                parentRespuestaCorta = loader.load();

                Scene sceneRespuestaCorta = new Scene(parentRespuestaCorta);

                FXMLEscena4Controller controlador = loader.getController();

                controlador.initData(sistemaPrincipal, numero);

                window.setScene(sceneRespuestaCorta);
                window.show();
            }
        } else {

            FXMLLoader loader = new FXMLLoader();

            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            loader.setLocation(classLoader.getResource("FXMLPreguntaRespondida.fxml"));

            Parent parentPreguntaRespondida;

            parentPreguntaRespondida = loader.load();

            Scene scenePreguntaRespondida = new Scene(parentPreguntaRespondida);

            FXMLPreguntaRespondidaController controlador = loader.getController();

            controlador.initData(sistemaPrincipal, numero);

            window.setScene(scenePreguntaRespondida);
            window.show();
        }
    }

}
