
import com.jfoenix.controls.JFXButton;
import dominio.Pregunta;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 * FXML Controller class
 *
 * Valentin SanchezBarre, Valentin Azor, Luca Cafalli
 */
public class FXMLRankingFinalController extends Ventana implements Initializable {

    private Sistema sistemaPrincipal;

    @FXML
    Label lblResultado,lblTotalPreguntas;

    @FXML
    JFXButton q1, q2, q3, q4, q5, q6, q7, q8, q9, q10;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    
    @FXML
    private void presionarBotonesSuperiores(ActionEvent event) throws IOException {

        JFXButton botonPresionado = (JFXButton) event.getSource();

        int numeroDeseado = Integer.parseInt(botonPresionado.getText());

        setearBotones();

        Stage stage = (Stage) q1.getScene().getWindow();

        mostrarEscena(stage, numeroDeseado,sistemaPrincipal);
    }

    //setea los botones superiores, con la informacion del sistema
  private void setearBotones() {


        for (int i = 0; i < sistemaPrincipal.getListaPreguntas().size(); i++) {
            
           Pregunta unaPregunta = sistemaPrincipal.getListaPreguntas().get(i);
           pintarBoton(i,unaPregunta.getColor());

            if (Integer.parseInt(q1.getText()) == i) {
                q1.setVisible(true);
            }
            if (Integer.parseInt(q2.getText()) == i) {

                q2.setVisible(true);
            }
            if (Integer.parseInt(q3.getText()) == i) {
                q3.setVisible(true);
            }
            if (Integer.parseInt(q4.getText()) == i) {
                q4.setVisible(true);
            }
            if (Integer.parseInt(q5.getText()) == i) {
                q5.setVisible(true);
            }
            if (Integer.parseInt(q6.getText()) == i) {
                q6.setVisible(true);
            }
            if (Integer.parseInt(q7.getText()) == i) {
                q7.setVisible(true);
            }
            if (Integer.parseInt(q8.getText()) == i) {
         
                q8.setVisible(true);
            }
            if (Integer.parseInt(q9.getText()) == i) {
              
                q9.setVisible(true);
            }
            if (Integer.parseInt(q10.getText()) == i) {
        
                q10.setVisible(true);
            }
        }
    }
  //pinta un boton a eleccion
   private void pintarBoton(int numero, String color1) {
       
       String color = "-fx-background-color:"+color1+";";

        if (numero == 0) {
            q1.setStyle(color);
        }
        if (numero == 1) {
            q2.setStyle(color);
        }
        if (numero == 2) {
            q3.setStyle(color);
        }
        if (numero == 3) {
            q4.setStyle(color);
        }
        if (numero == 4) {
            q5.setStyle(color);
        }
        if (numero == 5) {
            q6.setStyle(color);
        }
        if (numero == 6) {
            q7.setStyle(color);
        }
        if (numero == 7) {
            q8.setStyle(color);
        }
        if (numero == 8) {
            q9.setStyle(color);
        }
        if (numero == 9) {
            q10.setStyle(color);
        }

    }

    //Inicia los datos de la ventana, tomando de sistema
    public void initData(Sistema sistema) throws IOException {

        sistemaPrincipal = sistema;

        setearBotones();

        lblResultado.setText(Integer.toString(sistemaPrincipal.getCantidadPreguntasCorrectas()));
        
        lblTotalPreguntas.setText(Integer.toString(sistemaPrincipal.getCantidadPreguntasRespondidas()));
        
        //crearPDF(sistemaPrincipal);
        
        


    }

    //crea el pdf con la informacion del juego
     public void crearPDF(Sistema s) throws IOException {
        int cont = 780;
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);
            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {

                PDFont font = PDType1Font.COURIER;
                contents.setFont(font, 6);

                for (int i = 0; i < s.getListaPreguntas().size(); i++) {

                    contents.beginText();
                    contents.moveTextPositionByAmount(0, cont);

                    contents.drawString(s.getListaPreguntas().get(i).toString());

                    contents.endText();
                    cont = cont - 20;
                }

            }
            doc.save("PdfPrueba.pdf");
        }

    }
}
