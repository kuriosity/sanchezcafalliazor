

import com.jfoenix.controls.JFXButton;
import dominio.Pregunta;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Valentin Sanchez, Valentin Azor, Luca Cafalli
 */
public class FXMLEscena4Controller extends Ventana implements Initializable {

    private Sistema sistemaPrincipal;
    private Pregunta preguntaPrincipal;
    private int numero;
    private ArrayList<String> posiblesRespuestas;
    private String respuestaPrincipal;
    private String respuestaUsuario;

    @FXML
    Label pregunta, lblRespuestaCorrecta, lblMostrarRespuesta;

    @FXML
    TextField respuestaIngresada;

    @FXML
    JFXButton q1, q2, q3, q4, q5, q6, q7, q8, q9, q10;

    @FXML
    ImageView imgGif, loserGif;

    @FXML
    Button btnConfirmar,btnResultados;
    
    
    //Inicia los datos de la ventana, tomando de sistema
    public void initData(Sistema sistema, int contador) {

        sistemaPrincipal = sistema;
        preguntaPrincipal = sistemaPrincipal.getListaPreguntas().get(contador);
        numero = contador;
        posiblesRespuestas = preguntaPrincipal.getOpciones();
        respuestaPrincipal = posiblesRespuestas.get(0);
        preguntaPrincipal.setRespuesta(respuestaPrincipal);
        respuestaUsuario = preguntaPrincipal.getRespuestaUsuario();

        pregunta.setText(preguntaPrincipal.getPregunta());
        lblMostrarRespuesta.setText(respuestaPrincipal);

        setearBotones();
     
        respuestaIngresada.setStyle("-fx-text-fill: black; -fx-font-size: 16px;");
        
         if(sistemaPrincipal.getCantidadPreguntasRespondidas()==sistemaPrincipal.getListaPreguntas().size()-1){
            btnResultados.setVisible(true);
        }

    }

    //setea los botones tomando informacion del sistema, pintandolos
    private void setearBotones() {


        for (int i = 0; i < sistemaPrincipal.getListaPreguntas().size(); i++) {
            
           Pregunta unaPregunta = sistemaPrincipal.getListaPreguntas().get(i);
           pintarBoton(i,unaPregunta.getColor());

            if (Integer.parseInt(q1.getText()) == i) {
                q1.setVisible(true);
            }
            if (Integer.parseInt(q2.getText()) == i) {

                q2.setVisible(true);
            }
            if (Integer.parseInt(q3.getText()) == i) {
                q3.setVisible(true);
            }
            if (Integer.parseInt(q4.getText()) == i) {
                q4.setVisible(true);
            }
            if (Integer.parseInt(q5.getText()) == i) {
                q5.setVisible(true);
            }
            if (Integer.parseInt(q6.getText()) == i) {
                q6.setVisible(true);
            }
            if (Integer.parseInt(q7.getText()) == i) {
                q7.setVisible(true);
            }
            if (Integer.parseInt(q8.getText()) == i) {
         
                q8.setVisible(true);
            }
            if (Integer.parseInt(q9.getText()) == i) {
              
                q9.setVisible(true);
            }
            if (Integer.parseInt(q10.getText()) == i) {
        
                q10.setVisible(true);
            }
        }
    }

    public void setPregunta(String pregunta) {
        this.pregunta.setText(pregunta);
    }

    //accion al presionar boton confirmar, luego de ingresar una respuesta corta
    @FXML
    public void presionarConfirmar(ActionEvent event) throws IOException {

        String respuesta = respuestaIngresada.getText();
        respuestaUsuario = respuesta;

        sistemaPrincipal.setCantidadPreguntasRespondidas(sistemaPrincipal.getCantidadPreguntasRespondidas() + 1);

        preguntaPrincipal.setRespondida(true);

        preguntaPrincipal.setRespuestaUsuario(respuestaUsuario);
        
        
//accionar cambia segun si la respuesta es correcta o incorrecta
        if (posiblesRespuestas.contains(respuestaUsuario.toLowerCase())) {

            btnConfirmar.setDisable(true);

            imgGif.setVisible(true);

            playSound("src\\imagenes\\audioFFVII.mp3");

            pregunta.setText("Correcto!");

            pintarBoton(numero, "#26BD31");

            preguntaPrincipal.setColor("#26BD31");
            
            sistemaPrincipal.setCantidadPreguntasCorrectas(sistemaPrincipal.getCantidadPreguntasCorrectas()+1);

        } else {

            btnConfirmar.setDisable(true);

            playSound("src\\imagenes\\loseFF.mp3");

            pregunta.setText("Incorrecto...");

            loserGif.setVisible(true);

            pintarBoton(numero, "#EA2019");

            preguntaPrincipal.setColor("#EA2019");

            lblRespuestaCorrecta.setVisible(true);

            lblMostrarRespuesta.setVisible(true);

        }

    }

 
    //pinta un boton a eleccion con un color en formato #
   private void pintarBoton(int numero, String color1) {
       
       String color = "-fx-background-color:"+color1+";";

        if (numero == 0) {
            q1.setStyle(color);
        }
        if (numero == 1) {
            q2.setStyle(color);
        }
        if (numero == 2) {
            q3.setStyle(color);
        }
        if (numero == 3) {
            q4.setStyle(color);
        }
        if (numero == 4) {
            q5.setStyle(color);
        }
        if (numero == 5) {
            q6.setStyle(color);
        }
        if (numero == 6) {
            q7.setStyle(color);
        }
        if (numero == 7) {
            q8.setStyle(color);
        }
        if (numero == 8) {
            q9.setStyle(color);
        }
        if (numero == 9) {
            q10.setStyle(color);
        }

    }

   //accionar al presionar los botones superiores de camino, muestra la ventana correspondiente
    @FXML
    private void presionarBotonesSuperiores(ActionEvent event) throws IOException {

        JFXButton botonPresionado = (JFXButton) event.getSource();

        int numeroDeseado = Integer.parseInt(botonPresionado.getText());

        setearBotones();

        Stage stage = (Stage) q1.getScene().getWindow();

        mostrarEscena(stage, numeroDeseado,sistemaPrincipal);
    }

   
    //Accionar del boton Resultados, muestra los resultados finales
    @FXML
    public void clickResultados(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("FXMLRankingFinal.fxml"));

        Parent parentRankingFinal;

        parentRankingFinal = loader.load();

        Scene sceneRankingFinal = new Scene(parentRankingFinal);

        FXMLRankingFinalController controlador = loader.getController();

        controlador.initData(sistemaPrincipal);
        
        Stage window = (Stage) q1.getScene().getWindow();
        
          Sistema.crearPDF(sistemaPrincipal);

        window.setScene(sceneRankingFinal);
        window.show();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
