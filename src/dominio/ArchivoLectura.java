package dominio;

import java.io.*;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 *
 * @author Valentin Sanchez, Valentin Azor, Luca Cafalli
 */
public class ArchivoLectura {

    private String linea;
    private Scanner in;

    public ArchivoLectura(String unNombre) {
        try {
            in = new Scanner(Paths.get(unNombre));

        } catch (IOException e) {
            System.err.println("Error ");
            System.exit(1);
        }

    }

    public ArchivoLectura() {
        linea = "";

    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public void setIn(Scanner in) {
        this.in = in;
    }

    public String getLinea() {
        return linea;
    }

    public Scanner getIn() {
        return in;
    }

    public ArchivoLectura(String linea, Scanner in) {
        this.linea = linea;
        this.in = in;
    }

    public boolean hayMasLineas() {

        boolean hay = false;

        if (in.hasNext()) {
            linea = in.nextLine();
            hay = true;
        }

        return hay;

    }

    public String linea() {
        return linea;

    }

    public void cerrar() {
        in.close();
    }
}
