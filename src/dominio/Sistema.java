package dominio;

import java.util.ArrayList;
import dominio.Pregunta;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 *
 * @author Valentin Sanchez, Valentin Azor, Luca Cafalli
 */
public class Sistema {

    private int cantidadPreguntasRespondidas = 0;
    private int cantidadPreguntasCorrectas = 0;

    public void setCantidadPreguntasCorrectas(int cantidadPreguntasCorrectas) {
        this.cantidadPreguntasCorrectas = cantidadPreguntasCorrectas;
    }

    public int getCantidadPreguntasCorrectas() {
        return cantidadPreguntasCorrectas;
    }

    public int getCantidadPreguntasRespondidas() {
        return cantidadPreguntasRespondidas;
    }

    public void setCantidadPreguntasRespondidas(int cantidadPreguntasRespondidas) {
        this.cantidadPreguntasRespondidas = cantidadPreguntasRespondidas;
    }

    public void setListaPreguntas(ArrayList<Pregunta> listaPreguntas) {
        this.listaPreguntas = listaPreguntas;
    }

    public ArrayList<Pregunta> getListaPreguntas() {
        return listaPreguntas;
    }

    ArrayList<Pregunta> listaPreguntas = new ArrayList();

    public void tipoPregunta(String path) {

        ArchivoLectura arch = new ArchivoLectura(path);

        while (arch.hayMasLineas()) {
            String linea = arch.linea();
            Pregunta p = new Pregunta();
            if (arch.linea().contains("{T}")) {
                verdadero(p, linea);

            }
            if (arch.linea().contains("{F}")) {
                falso(p, linea);
            }
            if (arch.linea().contains("=") && arch.linea().contains("~")) {
                multOpcion(p, linea);
            }
            if (arch.linea().contains("=") && !arch.linea().contains("~")) {
                respCorta(p, linea);

            }
        }

    }

    public void verdadero(Pregunta p, String linea) {
        p.setVerd(true);
        p.setRespuesta("Verdadero");
        String[] resp = linea.split(" ");
        String P = " ";
        for (int i = 0; i < resp.length - 1; i++) {
            P = P + " ";
            P = P + resp[i];
        }
        p.setPregunta(P);

        this.listaPreguntas.add(p);
    }

    public void falso(Pregunta p, String linea) {
        p.setVerd(true);
        p.setRespuesta("Falso");
        String[] resp = linea.split(" ");
        String P = " ";
        for (int i = 0; i < resp.length - 1; i++) {
            P = P + " ";
            P = P + resp[i];
        }
        p.setPregunta(P);
        this.listaPreguntas.add(p);
    }

    public void multOpcion(Pregunta p, String linea) {
        p.setMultOp(true);
        String[] preg = linea.split("\\{");
        p.setPregunta(preg[0]);
        String[] resp = linea.split(" ");

        for (int i = 1; i < resp.length; i++) {
            if (resp[i].contains("=")) {
                resp[i] = resp[i].replace("=", "");
                p.setRespuesta(resp[i]);
                p.getOpciones().add(resp[i]);
            }
            if (resp[i].contains("~")) {
                resp[i] = resp[i].replace("~", "");
                p.getOpciones().add(resp[i]);

            }

        }
        p.setOpcion1(p.getOpciones().get(0));
        p.setOpcion2(p.getOpciones().get(1));
        p.setOpcion3(p.getOpciones().get(2));
        p.setOpcion4(p.getOpciones().get(3));
        if (p.getOpcion4().contains("}")) {
            p.setOpcion4(p.getOpcion4().replace("}", ""));
        }
        this.listaPreguntas.add(p);

    }

    public void respCorta(Pregunta p, String linea) {
        p.setRespCorta(true);
        String[] preg = linea.split("\\{");
        p.setPregunta(preg[0]);

        String op = preg[1];
        String[] op2 = op.split(" ");

        for (int i = 0; i < op2.length; i++) {
            if (op2[i].contains("=")) {
                op2[i] = op2[i].replace("=", "");
            }
            if (op2[i].contains("}")) {
                op2[i] = op2[i].replace("}", "");
            }

            p.getOpciones().add(op2[i]);

        }
        this.listaPreguntas.add(p);
    }

    public static void crearPDF(Sistema s) throws IOException {
        int cont = 780;
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);
            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {

                PDFont font = PDType1Font.COURIER;
                contents.setFont(font, 6);

                for (int i = 0; i < s.getListaPreguntas().size(); i++) {

                    contents.beginText();
                    contents.moveTextPositionByAmount(0, cont);

                    contents.drawString(s.getListaPreguntas().get(i).toString());

                    contents.endText();
                    cont = cont - 20;
                }

            }
            doc.save("PdfPrueba.pdf");
        }

    }

}
