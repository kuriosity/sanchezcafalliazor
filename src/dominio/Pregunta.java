/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.*;
import java.util.Scanner;
import dominio.Sistema;
import java.util.ArrayList;

/**
 *
 * @author valentin
 */
public class Pregunta {

    public boolean verd = false;
    public boolean multOp = false;
    public boolean respCorta = false;
    public String pregunta;
    public String respuesta;
    public String opcion1;
    public String opcion2;
    public String opcion3;
    public String opcion4;
    public String color = "";

   

    public ArrayList<String> opciones = new ArrayList();
    public boolean respondida;
    public String respuestaUsuario;

    public void setRespuestaUsuario(String respuestaUsuario) {
        this.respuestaUsuario = respuestaUsuario;
    }

    public String getRespuestaUsuario() {
        return respuestaUsuario;
    }

    public boolean isRespondida() {
        return respondida;
    }

    public void setRespondida(boolean respondida) {
        this.respondida = respondida;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setOpcion1(String opcion1) {
        this.opcion1 = opcion1;
    }

    public void setOpcion2(String opcion2) {
        this.opcion2 = opcion2;
    }

    public void setOpcion3(String opcion3) {
        this.opcion3 = opcion3;
    }

    public void setOpcion4(String opcion4) {
        this.opcion4 = opcion4;
    }

    public void setIn(Scanner in) {
        this.in = in;
    }

    public String getOpcion1() {
        return opcion1;
    }

    public String getOpcion2() {
        return opcion2;
    }

    public String getOpcion3() {
        return opcion3;
    }

    public String getOpcion4() {
        return opcion4;
    }

    public Scanner getIn() {
        return in;
    }

    public boolean isVerd() {
        return verd;
    }

    public void setVerd(boolean verd) {
        this.verd = verd;
    }

    public boolean isMultOp() {
        return multOp;
    }

    public void setMultOp(boolean multOp) {
        this.multOp = multOp;
    }

    public boolean isRespCorta() {
        return respCorta;
    }

    public void setRespCorta(boolean respCorta) {
        this.respCorta = respCorta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public ArrayList<String> getOpciones() {
        return opciones;
    }

    public void setOpciones(ArrayList<String> opciones) {
        this.opciones = opciones;
    }

    Scanner in = new Scanner(System.in);

    @Override
    public String toString() {

        if(this.isVerd()){
        return "Pregunta: "+this.getPregunta() + " Respuesta: "+this.getRespuesta() + " Resp usuario:"+this.getRespuestaUsuario();
        }
        if(this.isMultOp()){
        return "Pregunta: "+this.getPregunta()
                +" Respuesta: "+this.getRespuesta()+" OP1 : "+this.getOpcion1()+" OP2 :"+this.getOpcion2()+" OP3: "+this.getOpcion3()+" OP4 "+this.getOpcion4()+ " Resp usuario:"+this.getRespuestaUsuario();
        }
        if(this.isRespCorta()){
        return"Pregunta: "+this.getPregunta()+" Opciones: "+this.getOpciones()+ " Resp usuario:"+this.getRespuestaUsuario();
        }
        else{
        return null;
        }
    }


    public static void main(String[] args) throws IOException {
        Sistema s = new Sistema();
        s.tipoPregunta("pregunta gift.txt");
        s.crearPDF(s);
       
    }
}

